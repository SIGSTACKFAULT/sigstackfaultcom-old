# if it's the future and staticjinja doesn't work anymore idk write a flask app or something

UIKIT_VER=@3.5.9
UIKIT_DIST=https://cdn.jsdelivr.net/npm/uikit$(UIKIT_VER)/dist
STATICJINJA=./venv/bin/staticjinja

VENV=venv
STATICJINJA=$(VENV)/bin/staticjinja
NPM=npm --silent
LESSC=node_modules/less/bin/lessc
.PHONY: all clean cleaner
.SUFFIXES:


all: build build/static 

clean:
	rm -rf build uikit

cleaner: clean
	rm -rf node_modules venv

%/: # any directory
	mkdir -p $@

build: $(shell find src/templates) | build/ $(STATICJINJA)
	$(STATICJINJA) build --srcpath src/templates --outpath $@
	@touch build


build/static: build/static/ $(shell find src/static) build/static/css/style.css
	cp -r src/static/* build/static/
	@touch build/static

node_modules:
	$(NPM) install 

$(STATICJINJA): venv

venv:
	virtualenv -q $(VENV)
	$(VENV)/bin/pip install -qr requirements.txt

#build/static/uikit.min.js: node_modules | build/static/js/
#	curl -s $(UIKIT_DIST)/js/uikit.min.js > $@ #TODO: build from source

#build/static/uikit-icons.min.js: node_modules | build/static/js/
#	curl -s $(UIKIT_DIST)/js/uikit-icons.min.js > $@ #TODO: build from source 

#build/static/uikit/uikit.min.css: node_modules/uikit | build/static/uikit/
#	curl -s $(UIKIT_DIST)/css/uikit.min.css > $@

build/static/css/%.css: src/less/%.less src/less/variables.less | node_modules build/static/css/
	$(LESSC) --clean-css $< $@

