// https://stackoverflow.com/a/30810322/2729876

function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement("textarea");
  textArea.value = text;

  // Avoid scrolling to bottom
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Fallback: Copying text command was ' + msg);
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}

function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).then(function () {
    console.log('Async: Copying to clipboard was successful!');
  }, function (err) {
    console.error('Async: Could not copy text: ', err);
  });
}

function copyNotification(id){
  // appear then fade out
  // adapted from https://stackoverflow.com/questions/29017379/
  var target = document.getElementById(id)
  console.log("copyNotification:", target)
  target.style.display="inline-block";
  target.style.opacity=1;
  var effect = setInterval(function(){
    if (target.style.opacity > 0) {
      target.style.opacity -= 0.025;
    } else {
      target.style.display="none";
      clearInterval(effect);
    }
  }, 50);
}