// if anyone knows a better way of doing this, i'd love to hear about it 
// sigstackfault@gmail.com

var header = document.getElementsByTagName("header")[0]
var rad = document.getElementById("rad")


function sbin_mouseenterhandler(){
    //console.log("starting");
    rad.removeEventListener("animationiteration", sbin_iterationeventhandler);
    rad.classList.add("sbinalla")
}

function sbin_mouseleavehandler(){
    rad.addEventListener("animationiteration", sbin_iterationeventhandler)
}

function sbin_iterationeventhandler(){
    //console.log("stopping");
    rad.classList.remove("sbinalla")
    rad.removeEventListener("animationiteration", sbin_iterationeventhandler);
}


header.addEventListener("mouseenter", sbin_mouseenterhandler)
header.addEventListener("mouseleave", sbin_mouseleavehandler)

sbin_mouseenterhandler()
sbin_mouseleavehandler()